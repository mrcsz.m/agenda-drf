from django.contrib import admin

from .models import Contato

@admin.register(Contato)
class ContatoAdmin(admin.ModelAdmin):
    list_display = ['nome', 'telefone', 'email']
    list_display_links = list_display
    search_fields = list_display
    list_per_page = 40