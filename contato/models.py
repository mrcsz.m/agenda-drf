from django.db import models

class Contato(models.Model):
    nome = models.CharField(max_length=255, verbose_name='Nome', unique=True)
    telefone = models.CharField(max_length=11, verbose_name='Telefone')
    email = models.CharField(max_length=255, verbose_name='Email')

    def __str__(self):
        return self.nome

    class Meta:
        verbose_name = "Contato"
        verbose_name_plural = "Contatos"