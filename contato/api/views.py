from rest_framework import viewsets

from .serializers import ContatoSerializer
from ..models import Contato

class ContatoViewSet(viewsets.ModelViewSet):
    queryset = Contato.objects.all().order_by('nome')
    serializer_class = ContatoSerializer