from django.test import TestCase

from model_mommy import mommy

from ..models import Contato, Email, Endereco, Telefone

class TestContato(TestCase):
    def setUp(self):
        self.contato = mommy.make(Contato, nome="João")

    def test_contato_creation(self):
        self.assertTrue(isinstance(self.contato, Contato))
        self.assertEquals(self.contato.__str__(), self.contato.nome)

class TestEmail(TestCase):
    def setUp(self):
        self.email = mommy.make(Email, text="email@email.com")

    def test_email_creation(self):
        self.assertTrue(isinstance(self.contato, Contato))
        self.assertEquals(self.contato.__str__(), self.contato.nome)